# QR Code Scanner
- An updated version library that fixes the crashes when scanning barcode
- Version updated: 4.1.0
- Android Min SDK Supported: 14

# Credits
* Source: https://github.com/juliuscanute/qr_code_scanner